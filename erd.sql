-- Up
create table if not exists "user" (
  id serial primary key,
  email varchar(256) not null,
  password_hash char(60) null,
  token char(6) null,
  token_expire_at timestamp null,
  token_attempt integer null
);
-- Down
drop table if exists user;