import express from 'express'
import nodemailer from 'nodemailer'
import { client } from '../db'
import { env } from '../env'
import { hashPassword, comparePassword } from '../hash'

let transport = nodemailer.createTransport({
  service: 'hotmail',
  auth: {
    user: env.EMAIL_USER,
    pass: env.EMAIL_PASSWORD,
  },
})

let routes = express.Router()

const MINUTE = 60 * 1000

routes.post('/users', async (req, res) => {
  let { email, password } = req.body
  if (!email) {
    res.status(400)
    res.json({ error: 'missing email' })
    return
  }
  if (!password) {
    res.status(400)
    res.json({ error: 'missing password' })
    return
  }
  try {
    let password_hash = await hashPassword(password)

    let result = await client.query(
      /* sql */ `
			insert into "user" (email, password_hash) values ($1,$2)
			returning id
		`,
      [email, password_hash],
    )
    let id = result.rows[0].id
    res.json({ id })
  } catch (error) {
    res.status(500)
    res.json({ error: 'failed to save user: ' + error })
  }
})

routes.post('/users/login', async (req, res) => {
  let { email, password } = req.body
  if (!email) {
    res.status(400)
    res.json({ error: 'missing email' })
    return
  }
  if (!password) {
    res.status(400)
    res.json({ error: 'missing password' })
    return
  }
  try {
    let result = await client.query(
      /* sql */ `
			select id, password_hash
			from "user"
			where email = $1
		`,
      [email],
    )
    let user = result.rows[0]
    if (!user) {
      res.status(404)
      res.json({ error: 'this email is not registered' })
      return
    }
    let isMatched = await comparePassword({
      password,
      password_hash: user.password_hash,
    })
    if (!isMatched) {
      res.status(400)
      res.json({ error: 'wrong email or password' })
      return
    }
    res.json({ id: user.id })
  } catch (error) {
    res.status(500)
    res.json({ error: 'failed to save user: ' + error })
  }
})

routes.post('/users/request-reset-password', async (req, res) => {
  let { email } = req.body
  if (!email) {
    res.status(400)
    res.json({ error: 'missing email' })
    return
  }

  let code = Math.random().toString(36).slice(2, 8).toUpperCase()
  let expire = new Date(Date.now() + 15 * MINUTE)

  try {
    let result = await client.query(
      /* sql */ `
			select id from "user"
			where email = $1
		`,
      [email],
    )
    let user = result.rows[0]
    if (!user) {
      res.status(404)
      res.json({ error: 'this email is not registered' })
      return
    }

    await client.query(
      /* sql */ `
update "user"
set token = $1,
    token_expire_at = $2,
		token_attempt = 0
where id = $3
`,
      [code, expire, user.id],
    )
  } catch (error) {
    res.status(500)
    res.json({ error: 'Database error: ' + error })
    return
  }

  let text = `We received your request to reset password, please enter this verification code: ${code}. If you have not requested to reset password, it is safe to ignore this email.`
  let html = /* html */ `
<p>We received your request to reset password, please enter the verification code below</p>
<b>${code}</b>
<p>If you have not requested to reset password, it is safe to ignore this email.</p>
`

  try {
    await transport.sendMail({
      from: env.EMAIL_USER,
      to: email,
      subject: 'Reset Password of Demo Service',
      text,
      html,
    })
  } catch (error) {
    res.status(500)
    res.json({ error: 'Failed to send email: ' + error })
    return
  }

  res.json({
    message:
      'email sent, please check your inbox and also spam folder just in case',
  })
})

routes.post('/users/claim-reset-password', async (req, res) => {
  let { email, code, new_password } = req.body
  if (!email) {
    res.status(400)
    res.json({ error: 'missing email' })
    return
  }
  if (!code) {
    res.status(400)
    res.json({ error: 'missing code' })
    return
  }
  if (!new_password) {
    res.status(400)
    res.json({ error: 'missing new_password' })
    return
  }
  try {
    let result = await client.query(
      /* sql */ `
			select token_expire_at, token_attempt, id, token
			from "user"
			where email = $1
		`,
      [email],
    )
    let user = result.rows[0]
    if (!user) {
      res.status(404)
      res.json({ error: 'wrong email' })
      return
    }
    if (user.token_attempt > 5) {
      res.status(400)
      res.json({
        error:
          'verification code is expired, please request another code in the reset password page',
      })
      return
    }
    if (user.token !== code) {
      await client.query(
        /* sql */ `
				update "user"
				set token_attempt = token_attempt + 1
				where id = $1
			`,
        [user.id],
      )
      res.status(400)
      res.json({
        error:
          'wrong verification code, please double check the code from your email',
      })
      return
    }
    if (new Date(user.token_expired_at).getTime() < Date.now()) {
      res.status(400)
      res.json({
        error:
          'verification code expired, please request a new verification code',
      })
      return
    }
    let password_hash = await hashPassword(new_password)
    await client.query(
      /* sql */ `
		update "user"
		set password_hash = $1
		where id = $2
		`,
      [password_hash, user.id],
    )
    res.json({ message: 'the password is updated' })
  } catch (error) {
    res.status(500)
    res.json({ error: 'Database error: ' + error })
  }
})

export default routes
