import { config } from 'dotenv'
import populateEnv from 'populate-env'

config()

export let env = {
  EMAIL_USER: '',
  EMAIL_PASSWORD: '',
  DB_USER: '',
  DB_PASSWORD: '',
  DB_NAME: '',
}

populateEnv(env, { mode: 'halt' })
